<%-- 
    Document   : Asistwncias
    Created on : 18-Jun-2017, 6:57:10 PM
    Author     : Eduard QF
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="orm.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Asistensia</title>
        <link rel="stylesheet"  href="secondcss.css" type="text/css">
        <script>
            $(document).ready(function () {
                $('#curso').change(function (event) {
                    var $curso = $("select#curso").val();
                    $.get('reloadEstudiante', {asignaturass: $curso}, function (responseJson) {
                        var $select = $('#estudiantes');
                        $select.find('option').remove();
                        $.each(responseJson, function (key, value) {
                            $('<option>').val(key).text(value).appendTo($select);
                        });
                    });
                });
            });
        </script>
    </head>
    <body>
        <h1>Asistencia</h1>


        <div id="divcuerpo">
            <div>

                <form action="" method="post">
                    <select name="curso" id="curso">
                        <c:forEach var="curso" items="${cursos}">
                            <option value="${curso.idCurso}">
                                ${curso.idCurso}
                            </option>
                        </c:forEach>
                    </select>

                    <select name="estudiante" id="estudiantes">
                        <option>seleccione alumno</option>
                    </select>
                    <input type="submit" value="Buscar">
                </form><br><br>
            </div>
            <h3>Asistencias registradas</h3>
            <div id="asistencias">
                <div id="registro">
                    <table border="1" id="Tableregistro">
                        <tr id="title">
                            <td>
                                Fecha
                            </td>
                            <td>
                                Asistencia
                            </td>
                        </tr>
                        <c:forEach var="asistencia" items="${asistencias}">
                            <tr>
                                <td>
                                    ${asistencia.fecha}  
                                </td>
                                <td>
                                    <c:if test="${asistencia.asis==true}">
                                        presente
                                    </c:if>
                                    <c:if test="${asistencia.asis==false}">
                                        ausente
                                    </c:if>   
                                </td>
                            </tr>
                        </c:forEach>

                    </table>
                </div>
                <br>
                <div id="nuevasAsis">
                    <form method="post" action="CreateAsistencia">
                        <h3>Nueva asistencia:</h3>
                        <table>
                            <tr>
                            <tr>
                                <td>
                                    Estudiante:
                                </td>
                                <td>
                                    <select name="estudiante">
                                        <c:forEach var="estudiante" items="${alumnos}">
                                            <option value="${estudiante.persona_id_fk.nombre}" > ${estudiante.persona_id_fk.nombre}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                            <td>
                                asistencia:
                            </td>
                            <td>
                                <select id="asistencia" name="tipo">
                                    <option value="true">
                                        Presente
                                    </option>
                                    <option value="false">
                                        Ausente
                                    </option>
                                </select>
                            </td>
                            </tr>
                            <tr>
                                <td>
                                    fecha:
                                </td>
                                <td>
                                    <input id="fecha" type="date" name="fecha">
                                </td>
                            </tr>
                        </table>
                        <input type="submit" value="Crear">
                    </form>
                    <h3>${accion}</h3>
                </div>
                <br>
                <div id="deleteAsis">
                    <form method="post" action="DeleteAsistencia">
                        <h3>Eliminar asistencia</h3>
                        <table>
                            <tr>
                                <td>
                                    fecha:
                                </td>
                                <td>
                                    <select name="fecha">
                                        <c:forEach var="asistencia" items="${asistencias}">
                                            <option value="${asistencia.fecha}">
                                                ${asistencia.fecha}  
                                            </option>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Estudiante:
                                </td>
                                <td>
                                    <select name="estudiante">
                                        <c:forEach var="estudiante" items="${alumnos}">
                                            <option value="${estudiante.persona_id_fk.nombre}" > ${estudiante.persona_id_fk.nombre}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <input  type="submit" value="Eliminar">
                    </form>
                    <h3>${delete}</h3>
                </div>


            </div>
        </div>
        <a href="prinsipalWindows.jsp" id="volver">volver</a>
    </body>
</html>
