
<%-- 
    Document   : Anotaciones
    Created on : 18-Jun-2017, 4:55:04 PM
    Author     : Eduard QF
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="orm.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Anotaciones</title>
        <link rel="stylesheet"  href="secondcss.css" type="text/css">
    </head>
    <body>
        <h1>Anotaciones</h1>
        <div id="divcuerpo">
            <div><br><br></div>
            <div style="text-align: left">
                <form>
                    <select name="curso">
                        <c:forEach var="curso" items="${cursos}">
                            <option value="${curso.idCurso}">
                                ${curso.idCurso}
                            </option>
                        </c:forEach>
                    </select>

                    <select name="estudiante">
                        <c:forEach var="estudiante" items="${alumnos}">
                            <option value="${estudiante.persona_id_fk.nombre}" > ${estudiante.persona_id_fk.nombre}</option>
                        </c:forEach>
                    </select>
                    <input type="submit" value="Buscar">
                </form><br>

                <h3>anotaciones registradas</h3>
                <table border="1">

                    <c:forEach var="anotacion" items="${anotaciones}">
                        <tr>
                            <td>
                                <c:if test="${anotacion.tipo==true}">
                                    Positiva
                                </c:if>
                                <c:if test="${anotacion.tipo==false}">
                                    Negativa
                                </c:if>
                            </td>
                            <td>
                                ${anotacion.profesor_id_fk.persona_id_fk.nombre}
                            </td>
                            <td>
                                ${anotacion.detalle}
                            </td>
                        </tr>
                    </c:forEach>
                </table><br>
            </div>
            <div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br></div>

            <div style="text-align: right">
                <form action="CreateAnotacion" method="post">
                    <h3>Nueva anotacion:</h3>
                    <table>
                        <tr>
                            <td>
                                Estudiante:
                            </td>
                            <td>
                                <select id="estudiante" name="estudiante">
                                    <c:forEach var="estudiante" items="${alumnos}">
                                        <option value="${estudiante.persona_id_fk.nombre}" > ${estudiante.persona_id_fk.nombre}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                tipo:
                            </td>
                            <td>
                                <select id="tipo"  name="tipo">
                                    <option>
                                        Positiva
                                    </option>
                                    <option>
                                        Negativa
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                fecha:
                            </td>
                            <td>
                                <input id="fecha" type="text" name="fecha">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Profesor:
                            </td>
                            <td>
                                <select id="profesor" name="profesor">
                                    <c:forEach var="profesor" items="${docentes}">
                                        <option value="${profesor.persona_id_fk.nombre}"> ${profesor.persona_id_fk.nombre}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                detalle:
                            </td>
                            <td>
                                <input id="detalle" type="text" name="detalle">
                            </td>
                        </tr>
                    </table>
                    <input type="submit" value="Crear">
                </form><br>
                <h3>${respuestaC}</h3>
            </div>
            <div style="text-align: right">
                <form method="post" action="DeleteAnotacion">
                    <h3>Eliminar anotacion</h3>
                    <table>
                        <tr>
                            <td>
                                Estudiante:
                            </td>
                            <td>
                                <select id="estudiantes" name="estudiante">
                                    <c:forEach var="estudiante" items="${alumnos}">
                                        <option value="${estudiante.persona_id_fk.nombre}" > ${estudiante.persona_id_fk.nombre}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr><td>fecha:</td><td><input id="fecha" type="date" name="fecha"></td></tr>
                        <tr>
                            <td>
                                Profesor
                            </td>
                            <td>
                                <select id="profesor">
                                    <c:forEach var="profesor" items="${docentes}">
                                        <option value="${profesor.persona_id_fk.nombre}"> ${profesor.persona_id_fk.nombre}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <input  type="submit" value="Eliminar">
                </form>
                <h3>${respuestaD}</h3>
            </div>

        </div>
        <a href="prinsipalWindows.jsp" id="volver">volver</a>
    </body>
</div>
</html>
