<%-- 
    Document   : newProfesor
    Created on : 8-Jun-2017, 5:29:34 PM
    Author     : Eduard QF
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="orm.Profesor"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Profesores</title>
        <link rel="stylesheet"  href="secondcss.css" type="text/css">
    </head>
    <body>
        <h1>Profesor</h1>
        <div id="divcuerpo">
            <div>
                <h3>Profesores registrados</h3>
                <table border="1" style="width: 50%">
                    <tr>
                        <td>
                            nombre
                        </td>
                        <td>
                            rut
                        </td>

                    </tr>
                    <c:forEach var="profesor" items="${profes}">
                        <tr>
                            <td>
                                ${profesor.persona_id_fk.nombre}
                            </td>
                            <td>
                                ${profesor.persona_id_fk.rut}
                            </td>
                        </tr>
                    </c:forEach>

                </table>
            </div>
            <br>
            <div>
                <h3>crear Profesor</h3>
                <form action="CreateProfesor" method="get">
                    <table>
                        <tr>
                            <td>
                                Nombre:
                            </td>
                            <td>
                                <input type="text" name="nombre" >
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Apellido:
                            </td>
                            <td>
                                <input type="text" name="apellido" >
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Rut:
                            </td>
                            <td>
                                <input id="rut" type="number" name="rut" maxlength="8" > - <input id="digitoV" type="text" name="digitoV">
                            </td>
                        </tr>

                    </table>
                    <input type="submit" value="Crear">
                </form><br>
            </div>
            <div>
                <h3>Eliminar profesor:</h3>
                <form action="" method="get">
                    <table>
                        <tr>
                            <td>
                                Nombre:
                            </td>
                            <td>
                                <input type="text" name="nombre" >
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Apellido:
                            </td>
                            <td>
                                <input type="text" name="apellido" >
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Rut:
                            </td>
                            <td>
                                <input id="rut" type="number" name="rut"  maxlength="8"> - <input id="digitoV" type="text" name="digitoV">
                            </td>
                        </tr>

                    </table>
                    <input type="submit" value="Eliminar">
                </form>
            </div>
        </div>
        <a id="volver" href="prinsipalWindows.jsp">volver</a>
    </body>
</html>
