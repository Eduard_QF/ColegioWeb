<%-- 
    Document   : newActividad
    Created on : 17-Jun-2017, 11:44:41 PM
    Author     : Eduard QF
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Planificacion</title>
        <link rel="stylesheet"  href="secondcss.css" type="text/css">
        <script src="http://code.jquery.com/jquery-latest.min.js"></script>

        <script>
            $(document).ready(function () {
                $('#curso').change(function (event) {
                    var $curso = $("select#curso").val();
                    $.get('reloadAsignaturas', {asignaturass: $curso}, function (responseJson) {
                        var $select = $('#asignatura');
                        $select.find('option').remove();
                        $.each(responseJson, function (key, value) {
                            $('<option>').val(key).text(value).appendTo($select);
                        });
                    });
                });
            });
        </script>
    </head>
    <body>
        <h1>Planificasion</h1>
        <div id="divcuerpo">
            <div>
                <form action="ReloadActivities" method="post" name="actualizar">
                    <select name="curso" id="curso">
                        <c:forEach var="curso" items="${cursos}">
                            <option value="${curso.idCurso}">
                                ${curso.idCurso}
                            </option>
                        </c:forEach>
                    </select>

                    <select name="asignatura" id="asignatura" >
                        <option value="asignatura">
                            Lenguaje
                        </option>
                    </select>
                    <input type="submit" value="buscar">
                </form>
            </div>
            <div>
                <h3>Planificacion</h3>
                <table border="1px" id="actividades">
                    <tr>
                        <td>
                            Actividad
                        </td>
                        <td>
                            Fecha
                        </td>
                    </tr>
                    <c:forEach var="actividad" items="${actividades}">
                        <tr>
                            <td>
                                ${actividad.detalleAct}
                            </td>
                            <td>
                                ${actividad.fecha}
                            </td>
                        </tr>
                    </c:forEach>

                </table><br>
            </div>
            <div>
                <form action="" method="post">
                    <br><h3>Nueva Actividad</h3><br>
                    <table>

                        <tr>
                            <td>
                                detalle
                            </td>
                            <td>
                                <input type="text" name="detalle">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                fecha
                            </td>
                            <td>
                                <input type="datetime-local" name="fecha">
                            </td>
                        </tr>
                    </table>
                    <input type="submit" value="Crear">
                </form><br>
            </div>
            <div>
                <form action="" method="post">
                    <br><h3>Eliminar Actividad</h3><br>
                    <table>

                        <tr>
                            <td>
                                detalle
                            </td>
                            <td>
                                <input type="text" name="detalle">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                fecha
                            </td>
                            <td>
                                <input type="datetime-local" name="fecha">
                            </td>
                        </tr>
                    </table>
                    <input type="submit" value="Eliminar">
                </form>
            </div>
        </div>
        <a href="prinsipalWindows.jsp" id="volver">volver </a>

    </body>
</html>
