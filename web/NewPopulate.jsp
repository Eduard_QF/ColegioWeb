<%-- 
    Document   : NewPopulate
    Created on : 2-Jul-2017, 5:05:22 PM
    Author     : Eduard QF
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nuevo Poblamiento</title>
        <link rel="stylesheet"  href="secondcss.css" type="text/css">
    </head>
    <body>
        <h3>Nuevo Poblamiento</h3>
        <div>
            <form action="NewPopulate" method="POST">
                <table border="1">
                    <tr>
                        <td>
                            Nombre Institucion:
                        </td>
                        <td>
                            <input type="text" name="nombreI">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Cursos:
                        </td>
                        <td>
                            <input type="number" name="cursos" min="1">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Estudiantes:
                        </td>
                        <td>
                            <input type="number" name="estudiantes" min="0">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Asignaturas:
                        </td>
                        <td>
                            <input type="number" name="asignaturas" min="1">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Actividades:
                        </td>
                        <td>
                            <input type="number" name="actividades" min="0">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Asistencias:
                        </td>
                        <td>
                            <input type="number" name="asistencias" min="0">
                        </td>
                    </tr>
                </table>
                <input type="submit" value="Generar">
            </form>
        </div>
        <h3>${resultado}</h3>
        <a id="volver" href="prinsipalWindows.jsp">volver</a>
    </body>
</html>
