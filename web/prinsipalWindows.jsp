<%-- 
    Document   : prinsipalWindows
    Created on : 07-06-2017, 13:17:46
    Author     : Eduard QF
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title> Principal windows</title>
    <link rel="stylesheet"  href="prinsipalwStyle.css" type="text/css">
        
    </head>
    <body>
        <h3 id="bienvenida" > Bienvenido</h3>
        <div class="menu">
            <ul id="items">
                <li class="item1" ><p>Estudiantes</p>
                    <ul>
                        <li><a href="showApoderados">Administrar</a></li>    
                        <li><a href="showEvaluaciones">Evaluaciones</a></li>
                        <li><a href="showAnotaciones">Anotaciones</a></li>
                        <li><a href="showAsistencias">Asistencias</a></li>
                    </ul>
                </li>
                <li class="item2"><p >Curso</p>
                    <ul>
                        <li><a href="showcursos">Administrar</a></li>    
                    </ul>
                </li>

                <li class="item3"><p>Profesores</p>
                    <ul>
                        <li><a href="showProfesores">Administrar</a></li>    
                    </ul>
                </li>

                <li class="item4"><p>Apoderados</p>
                    <ul>
                        <li><a href="newapoderado.jsp">Administrar</a></li>    
                    </ul>
                </li>
                <li class="item5"><p >Asignaturas</p>
                    <ul>
                        <li><a href="showAsignaturas">Administrar</a></li>    
                        <li><a href="modificarAsignatura.jsp">Modificar</a></li>
                        <li><a href="showPlanificacion">Planificacion</a></li>
                    </ul>
                </li>
                <li class="item6"><p >Reportes</p>
                    <ul>
                        <li><a href="NewPopulate.jsp">Nuevo Poblamiento</a></li>
                        <li><a href="ShowReportes">Crear</a></li>    
                        <li><a href="">Mostrar Todos</a></li>
                    </ul>
                </li>

            </ul>
        </div>
        
    </body>
</html>
