<%-- 
    Document   : newapoderado
    Created on : 9-Jun-2017, 8:50:10 PM
    Author     : Eduard QF
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Crear Apoderado</title>
        <link rel="stylesheet"  href="secondcss.css" type="text/css">
    </head>
    <body>
        <h1>Crear Apoderado</h1>
        <form action="CreateApoderado" method="Post">
            <table>
                <tr>
                    <td>
                        Nombre:   
                    </td>
                    <td>
                        <input type="text" name="nombre" >
                    </td>
                </tr>
                <tr>
                    <td>
                        Apellido:
                    </td>
                    <td>
                        <input type="text" name="apellido" >
                    </td>
                </tr>
                <tr>
                    <td>
                        Rut:
                    </td>
                    <td>
                          <input id="rut" type="number" name="rut" > - <input id="digitoV" type="text" name="digitoV">
                    </td>
                </tr>
                
            </table>
            <input type="submit" value="Crear">
        </form>
        <br>
        <h1>Eliminar Apoderado</h1>
        <form action="CreateApoderado" method="Post">
            <table>
                <tr>
                    <td>
                        Nombre:   
                    </td>
                    <td>
                        <input type="text" name="nombre" >
                    </td>
                </tr>
                <tr>
                    <td>
                        Apellido:
                    </td>
                    <td>
                        <input type="text" name="apellido" >
                    </td>
                </tr>
                <tr>
                    <td>
                        Rut:
                    </td>
                    <td>
                          <input id="rut" type="number" name="rut" > - <input id="digitoV" type="text" name="digitoV">
                    </td>
                </tr>
                
            </table>
            <input type="submit" value="Eliminar">
        </form>
        
        <a id="volver" href="prinsipalWindows.jsp">volver</a>
    </body>
</html>
