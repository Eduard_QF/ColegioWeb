<%-- 
    Document   : newEstudante
    Created on : 8-Jun-2017, 3:48:36 PM
    Author     : Eduard QF
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="orm.*" %>>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet"  href="secondcss.css" type="text/css">

    </head>
    <body>
        <h1>Administrar Estudiante</h1>
        <div style="height: 190px">
            <h3>Crear Estudiante</h3>
            <form action ="CreateEstudiante" method="get" >
                <table>
                    <tr>
                        <td >
                            Nombre:  
                        </td>
                        <td colspan="2">
                            <input  type="text" name="firstname"  >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Apellido:
                        </td>
                        <td colspan="2">
                            <input  type="text" name="lastname"  >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Rut:
                        </td>
                        <td colspan="2">
                            <input id="rut" type="number" name="rut" > - <input id="digitoV" type="text" name="digitoV">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Curso:
                        </td>
                        <td colspan="2">

                            <select name="curso" >
                                <c:forEach var="curso" items="${cursos}">
                                    <option value="${curso.idCurso}">${curso.idCurso}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Apoderado:
                        </td>
                        <td>
                            <select id="apoderado" name="apoderado">
                                <c:forEach var="apoderado" items="${apoderados}">
                                    <option value="${apoderado.persona_id_fk.nombre}">
                                        ${apoderado.persona_id_fk.nombre}
                                    </option>
                                </c:forEach>
                            </select>
                        </td>
                        <td>
                            <a id="newApoderado" href="newapoderado.jsp">o crear nuevo apoderado</a>                    
                        </td>
                    </tr>
                </table>
                <br><input type="submit" value="Crear"/>
            </form>
            <h4>${result}</h4>
        </div>
        
        <br><br><br>
        <div>
            <h3>Eliminar Estudiante</h3>
            <form action="DeleteEstudiante" method="post" style="height: 110px">
                <table border="0">
                    <tr>
                        <td>Nombre</td>
                        <td><input type="text" name="nombre"></td>
                    </tr>
                    <tr>
                        <td>Rut</td>
                        <td><input id="rut" type="number" name="rut" > - <input id="digitoV" type="text" name="digitoV"></td>
                    </tr>
                </table>
                <input type="submit" value="Eliminar">
            </form>
            <h4>${resp}</h4>
        </div>
        <br>
        <div>
            <a id="volver" href="prinsipalWindows.jsp">volver</a>
        </div>
        
    </body>
</html>
