<%-- 
    Document   : modificarAsignatura
    Created on : 18-Jun-2017, 4:05:07 PM
    Author     : Eduard QF
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="modelo.Body"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet"  href="secondcss.css" type="text/css">
    </head>
    <body>
       

            <select name="curso" >
                <option>1 A</option>
                <option>1 B</option>
                <option>2 A</option>
                <option>2 B</option>
            </select>

            <select name="asignatura" >
                <option>Lenguaje</option>
                <option>Matematica</option>
                <option>Ingles</option>
                <option>Ciencias Sociales</option>
                <input type="submit" value="Buscar">
            </select>

        </form>

        <h3>Docente: </h3>
        <p>Nombre: ${nombre}</p>
        <p>Rut: ${rut}</p>
        
        <form action="" method="post">
        Cambia docente:
        <select>
            <option>docente 1</option>
            <option>docente 2</option>
            <option>docente 3</option>
        </select>
        <input type="submit" value="Modificar">
    </form>
    </body>
</html>
