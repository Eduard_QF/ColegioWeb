<%-- 
    Document   : newAsignatura
    Created on : 9-Jun-2017, 7:42:33 PM
    Author     : Eduard QF
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet"  href="secondcss.css" type="text/css">
    </head>
    <body>
        <h1>Administrar Asignaturas</h1>
        <div>
            <h3>Crear Asignatura</h3>
            <form action="CreateAsignatura" method="post">
                <table>
                    <tr>
                        <td>
                            Nombre Asignatura:   
                        </td>
                        <td>
                            <input type="text" name="asignatura" >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Profesor dedicado:
                        </td>
                        <td>
                            <select name="profesor">
                                <c:forEach var="profe" items="${docentes}">
                                    <option value="${profe.persona_id_fk.nombre}">
                                        ${profe.persona_id_fk.nombre}
                                    </option>
                                </c:forEach>

                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Curso:
                        </td>
                        <td>
                            <select name="curso">
                                <c:forEach var="curso" items="${cursos}">
                                    <option value="${curso.idCurso}">
                                        ${curso.idCurso}
                                    </option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                </table>
                <input type="submit" value="crear">
            </form>
            <h3>${actionC}</h3>
        </div>

        <div>
            <h3>Eliminar Asignatura</h3>
            <form action="DeleteAsignatura" method="post">
                <table>
                    <tr>
                        <td>
                            Curso:
                        </td>
                        <td>
                            <select name="curso">
                                <c:forEach var="curso" items="${cursos}">
                                    <option value="${curso.idCurso}">
                                        ${curso.idCurso}
                                    </option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Asignatura:
                        </td>
                        <td>
                            <select name="asignatura">
                                <c:forEach var="asign" items="${asignaturas}">
                                    <option value="${asign.nombreAsign}">${asign.nombreAsign}</option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                </table>
                <input type="submit" value="crear">
            </form
            <h3>${actionD}</h3>
        </div>
        <a id="volver" href="prinsipalWindows.jsp"> volver</a>
    </body>
</html>
