<%-- 
    Document   : Evaluaciones
    Created on : 21-Jun-2017, 12:05:39 AM
    Author     : Eduard QF
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="orm.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluaciones</title>
        <link rel="stylesheet"  href="secondcss.css" type="text/css">
    </head>
    <body>
        <div><h1>Evaluaciones</h1></div>
        <div id="divcuerpo">
            <div id="cursor">
                <div><br><br></div>
                <form action="" method="post">
                    <select name="curso">
                        <c:forEach var="curso" items="${cursos}">
                            <option value="${curso.idCurso}">
                                ${curso.idCurso}
                            </option>
                        </c:forEach>
                    </select>

                    <select name="estudiante">
                        <c:forEach var="estudiante" items="${alumnos}">
                            <option value="${estudiante.persona_id_fk.nombre}" > ${estudiante.persona_id_fk.nombre}</option>
                        </c:forEach>
                    </select>
                    <input type="submit" value="Buscar">
                </form><br><br>
            </div>

            <div id="registerEvaluacion">
                <h3>Evaluaciones registradas</h3>
                <table border="1" style="width: 33%">
                    <tr>
                        <td>
                            Actividad
                        </td>
                        <td>
                            Nota obtenida
                        </td>
                    </tr>
                    <c:forEach var="nota" items="${notas}">
                        <tr>
                            <td>
                                ${nota.actividad_id_fk1.detalleAct}  
                            </td>
                            <td>
                                ${nota.nota}  
                            </td>
                        </tr>
                    </c:forEach>
                </table><br>
            </div>
            <div id="createEvaluacion">
                <form action="createEvaluacion" method="post">
                    <h3>Nueva evaluacion:</h3>
                    <table border="1">
                        <tr>
                            <td>
                                Estudiante:
                            </td>
                            <td>
                                <select name="estudiante">
                                    <c:forEach var="estudiante" items="${alumnos}">
                                        <option value="${estudiante.persona_id_fk.nombre}" > ${estudiante.persona_id_fk.nombre}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Asignatura:
                            </td>
                            <td>
                                <select name="asignatura">
                                    <c:forEach var="asignatura" items="${asignaturas}">
                                        <option value="${asignatura.nombreAsign}">${asignatura.nombreAsign}</option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                Actividad:
                            </td>
                            <td>
                                <select id="tipo"  name="actividad">
                                    <c:forEach var="actividad" items="${actividades}">
                                        <option value="${actividad.detalleAct}"> ${actividad.detalleAct} </option>
                                    </c:forEach>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                nota:
                            </td>
                            <td>
                                <input id="nota" type="number" name="nota" min="1" max="7">
                            </td>
                        </tr>
                    </table>
                    <input type="submit" value="Añadir Nota">
                </form><br>
                <h3>${actionC}</h3>
            </div>
            <div id="deleteEvaluacion">
                <form action="deleteEvaluacion" method="post">
                    <h3>Eliminar evaluacion</h3>
                    <table>
                        <tr><td>fecha:</td><td><input id="fecha" type="date" name="fecha"></td></tr>
                        <tr><td>Estudiante:</td><td><input type="text" name="estudiante"></td></tr>
                    </table>
                    <input  type="submit" value="Eliminar">
                </form>
                <h3>${actionD}</h3>
            </div>

        </div>
        <a href="prinsipalWindows.jsp" id="volver">volver</a>
    </body>

</html>
