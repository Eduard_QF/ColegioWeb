<%-- 
    Document   : CreateReport
    Created on : 6-Jul-2017, 11:16:28 PM
    Author     : Eduard QF
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Crear Reportes</title>
        <link rel="stylesheet"  href="secondcss.css" type="text/css">
    </head>
    <body>
        <h1>Reportes</h1>
        <div>
            <h3>Reporte Apoderados</h3>
            <form>
                <table>
                    <tr>
                        <td>
                            Curso:   
                        </td>
                        <td>
                            <select>
                                <c:forEach var="curso" items="${cursos}">
                                    <option>
                                        ${curso}
                                    </option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                </table>
                <input type="submit" value="crear">
            </form>
        </div><br>
        <div>
            <h3>Reporte Asignatura</h3>
            <form>
                <table>
                    <tr>
                        <td>
                            Curso
                        </td>
                        <td>
                            <select>
                                <c:forEach var="curso" items="${cursos}">
                                    <option>
                                        ${curso}
                                    </option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Asignatura
                        </td>
                        <td>
                            <select>
                                <c:forEach var="asignatura" items="${asignaturas}">
                                    <option>
                                        ${asignatura}
                                    </option>
                                </c:forEach>
                            </select>
                        </td>
                    </tr>
                </table>
                <input type="submit" value="crear">
            </form>
        </div><br>
        <div>
            <h3>Reporte Asistencia-Notas </h3>
            <form>
                <table>
                    <tr>
                        <td>
                            Curso
                        </td>
                        <td>
                            <select>
                                <c:forEach var="curso" items="${cursos}">
                                    <option>
                                        ${curso}
                                    </option>
                                </c:forEach>     
                            </select>
                        </td>
                    </tr>
                </table>
                <input type="submit" value="Crear">
            </form>
        </div><br>
        <div>
            <h3>Reporte Asistencia Limite</h3>
            <form>
                <table>
                    <tr>
                        <td>
                            Curso
                        </td>
                        <td>
                            <select>
                                <c:forEach var="curso" items="${cursos}">
                                    <option>
                                        ${curso}
                                    </option>
                                </c:forEach>  
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Limite Asistencia
                        </td>
                        <td>
                            <input type="number" value="Crear" min="1" max="100">
                        </td>
                    </tr>
                </table>
                <input type="submit" value="Crear">
            </form>
        </div><br>
        <div>
            <h3>Reporte Reprobados Curso</h3>
            <form>
                <table>
                    <tr>
                        <td>
                            Curso
                        </td>
                        <td>
                            <select>
                                <c:forEach var="curso" items="${cursos}">
                                    <option>
                                        ${curso}
                                    </option>
                                </c:forEach>  
                            </select>
                        </td>
                    </tr>
                </table>
                <input type="submit" value="Crear">
            </form>
        </div><br>
        <div>
            <h3>Reporte Multiples Pupilos</h3>
            <form>

                <input type="submit" value="Crear">
            </form>
        </div>
        <div>
            <h3>Reporte Planificacion Estudiantes</h3>
            <form>
                <table>
                    <tr>
                        <td>
                            Apoderado
                        </td>
                        <td>
                            <select>
                                <c:forEach var="apoderado" items="${apoderados}">
                                    <option>
                                        ${apoderado}
                                    </option>
                                </c:forEach>  
                            </select>
                        </td>
                    </tr>
                </table>
                <input type="submit" value="Crear">
            </form>
        </div>
    </body>
</html>
