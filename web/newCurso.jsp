<%-- 
    Document   : newCurso
    Created on : 8-Jun-2017, 5:04:14 PM
    Author     : Eduard QF
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet"  href="secondcss.css" type="text/css">
    </head>
    <body>
        <h1>Cursos</h1>
        <div id="divcuerpo">



            <h3>Cursos existentes</h3>

            <table border='1' style="width: 50%">
                <c:forEach var="curso" items="${cursos}">
                    <tr>
                        <td>
                            <p>${curso.idCurso}<p>
                        </td>
                    </tr>
                </c:forEach>
            </table>
            <div> 
                <h3>Crear Curso</h3>

                <form action="CreatCurso" method="post">
                    <table>
                        <tr>
                            <td>
                                Numero:
                            </td>
                            <td>
                                <input type="number" name="numero" min="1" max="8" >
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Letra:
                            </td>
                            <td>
                                <input type="text" name="letra" >

                            </td>
                        </tr>
                    </table>
                    <input type="submit" value="Crear">
                </form> <p>${resultado}</p> <br>
            </div>
            <div>
                <h3>Eliminar Curso</h3>
                <form action="DeleteCurso" method="post">
                    <table>
                        <tr>
                            <td>
                                Numero:
                            </td>
                            <td>
                                <input type="number" name="numero" min="1" max="8" >
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Letra:
                            </td>
                            <td>
                                <input type="text" name="letra" >

                            </td>
                        </tr>
                    </table>
                    <input type="submit" value="Eliminar">
                </form>
            </div>

        </div>
        <a id="volver" href="prinsipalWindows.jsp">volver</a>
    </body>
</html>
