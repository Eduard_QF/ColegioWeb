/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shows;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import orm.Actividad;
import orm.Asignatura;
import orm.Nota;
import orm.Curso;
import orm.Estudiante;

/**
 *
 * @author Eduard QF
 */
@WebServlet(name = "showEvaluaciones", urlPatterns = {"/showEvaluaciones"})
public class showEvaluaciones extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        Curso[] cur = capanegocios.Read.readCursos();
        Estudiante[] alumnos = cur[0].estudiante.toArray();
        Nota[] notas = alumnos[0].nota.toArray();
        Asignatura[] asignaturas=cur[0].asignatura.toArray();
        Actividad []actividades=asignaturas[0].getPlanificacion().actividad.toArray();
        request.setAttribute("cursos", cur);
        request.setAttribute("alumnos", alumnos);
        request.setAttribute("notas", notas);
        request.setAttribute("asignaturas", asignaturas);
        request.setAttribute("actividades", actividades);
        RequestDispatcher dispatcher = request.getRequestDispatcher("Evaluaciones.jsp");
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
