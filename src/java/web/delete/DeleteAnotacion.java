/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.delete;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import orm.Anotaciones;
import orm.Curso;
import orm.Estudiante;
import orm.Profesor;

/**
 *
 * @author Eduard QF
 */
@WebServlet(name = "DeleteAnotacion", urlPatterns = {"/DeleteAnotacion"})
public class DeleteAnotacion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String fecha = request.getParameter("fecha");
        String nombre = request.getParameter("estudiante");
        Estudiante alumno = capanegocios.Read.readEstudiante(nombre);
        String respuestaD=null;
        if(capanegocios.Delete.deleteAnotacion(fecha, alumno)) {
            respuestaD="Anotacion eliminada correctamente";
        } else {
            respuestaD="Error al eliminar la anotacion";
        }
        
        Curso[] cur = capanegocios.Read.readCursos();
        Estudiante[] alumnos = cur[0].estudiante.toArray();
        Profesor profes[] = capanegocios.Read.readProfesoresCurso(cur[0].getIdCurso());
        Anotaciones[] anotaciones = alumnos[0].anotaciones.toArray();
        request.setAttribute("respuestaD",respuestaD);
        request.setAttribute("cursos", cur);
        request.setAttribute("alumnos", alumnos);
        request.setAttribute("anotaciones", anotaciones);
        request.setAttribute("docentes", profes);
        RequestDispatcher dispatcher = request.getRequestDispatcher("Anotaciones.jsp");
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
