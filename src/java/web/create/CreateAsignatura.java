/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.create;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.orm.PersistentException;
import orm.Asignatura;
import orm.Curso;
import orm.Profesor;

/**
 *
 * @author Eduard QF
 */
@WebServlet(name = "CreateAsignatura", urlPatterns = {"/CreateAsignatura"})
public class CreateAsignatura extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String nombre = request.getParameter("asignatura");
            orm.Curso curs = capanegocios.Read.readCurso(request.getParameter("curso"));
            orm.Profesor profesor = capanegocios.Read.readProfesor(request.getParameter("profesor"));
            String actionC;
            if (capanegocios.Create.createAsignatura(nombre, curs, profesor)) {
                actionC = "Asignatura creada correctsmente";

            } else {
                actionC = "Error al crear la asignatura";
            }

            Profesor[] docentes = capanegocios.Read.readProfesoresInstitucion();
            Curso[] cur = capanegocios.Read.readCursos();
            Asignatura[] asign = cur[0].asignatura.toArray();
            request.setAttribute("cursos", cur);
            request.setAttribute("docentes", docentes);
            request.setAttribute("asignaturas", asign);

            request.setAttribute("actionC", actionC);
            RequestDispatcher dispatcher = request.getRequestDispatcher("newAsignatura.jsp");
            dispatcher.forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
