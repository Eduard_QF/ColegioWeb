/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web.create;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import orm.*;

/**
 *
 * @author Eduard QF
 */
@WebServlet(name = "createEvaluacion", urlPatterns = {"/createEvaluacion"})
public class createEvaluacion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        float nota=Float.valueOf(request.getParameter("nota"));
        String act=request.getParameter("actividad");
        Actividad activ=null;
        String actionC;
        Estudiante alumno=capanegocios.Read.readEstudiante(request.getParameter("estudiante"));
        Curso[] cur = capanegocios.Read.readCursos();
        
        for (Curso curs:capanegocios.Read.readCursos()) {
            for (Asignatura asigns:curs.asignatura.toArray()) {
                for (Actividad activities:asigns.getPlanificacion().actividad.toArray()) {
                    if (act.equals(activities.getDetalleAct())) {
                        activ=activities;
                    }
                }
            }
        }
        
        if (capanegocios.Create.createNota(nota, activ, alumno)) {
            actionC="Nota ingresada correctamente";
        }else{
            actionC="Error al ingresar la nota";
        }   
        
        
        Estudiante[] alumnos = cur[0].estudiante.toArray();
        Nota[] notas = alumnos[0].nota.toArray();
        Asignatura[] asignaturas=cur[0].asignatura.toArray();
        Actividad []actividades=asignaturas[0].getPlanificacion().actividad.toArray();
        request.setAttribute("actionC", actionC);
        request.setAttribute("cursos", cur);
        request.setAttribute("alumnos", alumnos);
        request.setAttribute("notas", notas);
        request.setAttribute("asignaturas", asignaturas);
        request.setAttribute("actividades", actividades);
        RequestDispatcher dispatcher = request.getRequestDispatcher("Evaluaciones.jsp");
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
